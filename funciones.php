<?php
	//funciones
	function nomFuncion ($parametro1, $parametro2){
		//instrucciones
	}
	
	function saludo (){
			$saludar = "Buenas tardes";
			return $saludar;
			echo "Buenas tardes";
	}
	
	//llamado de la funciones
	//echo saludo ();
	function suma ($a,$b){
		return $a+$b;
	}
	
	//$a = 5;
	//$b =10;
	//echo suma(5,6);
	
	function bebida($tipo="Agua"){
		return "Mi bebida es : $tipo";
	}
	//echo bebida ("Soda");
	
	//parametros por valor
	function porValor($numero){
		$numero=10;
		//echo "Valor dentro de la funcion: $numero";
	}
	//echo "PARAMETRO POR VALOR <BR/>";
	$fValor=25;
	//echo porValor($fValor). "<br/>";
	//echo "Valor fuera de la funcion: $fValor";

	//parametro por referencia
	function porReferencia (&$var){
			$var=10;
			//echo "El valor dentro de la funcion es: $var";
	}
		
		//echo "<br>PARAMETRO POR REFERENCIA<BR>";
		$fValoR=25;
		//echo porReferencia($fValoR). "<br/>";
		//echo "Valor fuera de la funcion: $fValoR";
		
		//imprimir numero de funciones
		function mostrar ($array){
				foreach ($array as $datos){
					//echo $datos;
				}
		}
		
		$numeros=array(1,2,3,4,5,6);
		///mostrar($numeros);
		
		//func_num_args
		//func_get_arg
		
		function materias (){
			if(func_num_args()>0){
				$parametros = func_get_args();
				foreach ($parametros as $materias){
					//echo $materias . "<br/>";
				}
			}
		}
		
		//llamado a la funcion
		materias("Programacion IV", "Fundamentos");
		
		//crear una funcion que muestre la tabla
		//de multiplicar de un mumero
		
		
		function multiplicar ($numero){
			for ($i=1;$i<=10;$i++){
				echo "$numero x $i =".$numero*$i. "<br/>";
				
			}
		}
		
		//multiplicar (10);
?>
		<form action="respuesta.php" method="POST">
		Escriba un numero:
		<input type="text" name="numero"/>
		<input type="submit" value="multiplicar" name="enviar"/>
		</form>
